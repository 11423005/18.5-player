﻿#include <iostream>
#include <string>

using namespace std;// чтобы каждый раз не писать std

class Player // создаем класс Player
{
public:
    string name;
    int score;

    Player() : name(""), score(0) // конструктор по умолчанию: имя пустой строкой и количество очков с нулем
    {}
    Player(string n, int s) : name(n), score(s) // конструктор с параметрами для инициализации имени и количества очков
    {}
};

void bubblesort(Player** l, Player** r)  // Функция сортировки пузырьком
{
    int sz = r - l;
    if (sz <= 1) return;
    bool b = true;
    while (b)
    {
        b = false;
        for (Player** i = l; i + 1 < r; i++) 
        {
            if ((*i)->score < (*(i + 1))->score) 
            {
                swap(*i, *(i + 1));
                b = true;
            }
        }
        r--;
    }
}

int main() 
{
    int numofPlayers;
    cout << "Enter the number of players: ";
    cin >> numofPlayers;

    Player** players = new Player * [numofPlayers];

    for (int i = 0; i < numofPlayers; ++i) 
    {
        string name;
        int score;
        cout << "Enter name and score for player " << i + 1 << ": ";
        cin >> name >> score;
        players[i] = new Player(name, score);
    }

    bubblesort(players, players + numofPlayers);

    cout << "Name\tScore" << endl; // \t -горизонтальная табуляция (4-8 пробелов в зависимости от настроек)
    for (int i = 0; i < numofPlayers; ++i) 
    {
        cout << players[i]->name << "\t" << players[i]->score << endl;
        delete players[i];
    }
    delete[] players;

    return 0;
}
